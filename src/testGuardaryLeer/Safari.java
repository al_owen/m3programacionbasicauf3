/**
 * 
 */
package testGuardaryLeer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;

/**@author Álvaro Owen de la Quintana
 * ProgramacionBasicaUF3 
 * 21 feb 2023
 */
public class Safari {

	public static void main(String[] args) {
		Pokemon pokachu = new Pokemon("Pokachu", 100, 50, 50);//creamos Pokemons
		Pokemon dikorita = new Pokemon("Dikorita", 120,50,60);
		HashMap<String, Pokemon> pokemons = new HashMap<>();
		pokemons.put("Charvander", new Pokemon("Charvander", 120,70,80));
		pokemons.put("ducario", new Pokemon("ducario", 150,70,60));
		
		//Pack de escritura de objetos
		try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("res/pokedex.txt"))){
			oos.writeObject(pokachu);//guardamos los objetos
			oos.writeObject(dikorita);
			oos.writeObject(pokemons);
		}catch (Exception e) {
			System.out.println("ha petado, pero no!");
		}
		
		//File sirve para el control de ficheros y archivos
		File f = new File("res/pokedex.txt");
		if (f.exists()) {//solo entra si el archivo existe
			//pack de lectira de objetos
			try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(f))){
				Pokemon p = (Pokemon)ois.readObject();//lee un objeto y lo devuelve como un Object, por lo tanto deberemos castear
				Pokemon p2 = (Pokemon)ois.readObject();
				HashMap<String, Pokemon> pokemones = (HashMap<String, Pokemon>)ois.readObject();//podemos guardar maps o listas con todos sus valores 
				System.out.println(p);
				System.out.println(p2);
				for (Pokemon po : pokemones.values()) {//leemos la lista
					System.out.println(po);
				}
			} catch (FileNotFoundException e) {
				//e.printStackTrace();
				System.out.println("ha petado, pero sigue vivo, aunque el archivo no existe!");
			}catch (Exception e) {
				//e.printStackTrace();
				System.out.println("ha petado, pero no se porque, cosas...!");
			}
		}
		
		
	}
	
}
