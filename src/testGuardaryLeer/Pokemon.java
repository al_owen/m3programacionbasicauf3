/**
 * 
 */
package testGuardaryLeer;

import java.io.Serializable;

/**@author Álvaro Owen de la Quintana
 * ProgramacionBasicaUF3 
 * 21 feb 2023
 */
//para guardar un objeto debe implementar la clase serializable
public class Pokemon implements Serializable{

	//Debemos generar esta variable, para evitar problemas
	private static final long serialVersionUID = 1L;
	private String name;
	private int hp;
	private int atack;
	private int def;
	
	public Pokemon(String name, int hp, int atack, int def) {
		super();
		this.name = name;
		this.hp = hp;
		this.atack = atack;
		this.def = def;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getHp() {
		return hp;
	}
	public void setHp(int hp) {
		this.hp = hp;
	}
	public int getAtack() {
		return atack;
	}
	public void setAtack(int atack) {
		this.atack = atack;
	}
	public int getDef() {
		return def;
	}
	public void setDef(int def) {
		this.def = def;
	}
	@Override
	public String toString() {
		return "Pokemon [name=" + name + ", hp=" + hp + ", atack=" + atack + ", def=" + def + "]";
	}
	
	
	
	
}
