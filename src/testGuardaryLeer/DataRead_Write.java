/**
 * 
 */
package testGuardaryLeer;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author Álvaro Owen de la Quintana ProgramacionBasicaUF3 28 feb 2023
 */
public class DataRead_Write {
	
	public static void main(String[] args) {
		Pokemon p = new Pokemon("Darizard", 50, 20, 30);//creamos un objeto de tipo pokemon
		//creamos el pack de objetos de escritura de datos
		try (DataOutputStream dos = new DataOutputStream(new FileOutputStream("res/datos.dat"))){
			dos.writeUTF(p.getName());//escribimos el dato que queremos en el formato adecuado, en este caso String es UTF
			dos.writeInt(p.getAtack());//escribimos los integers con writeInt
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		//pack para poder leer datos
		try(DataInputStream dis = new DataInputStream(new FileInputStream("res/datos.dat"))) {
			//while (dis.available() > 0) { //en el caso de que necesitemos coger varios datos
				String nom = dis.readUTF();//leemos strings
				int atack = dis.readInt();//leemos ints
				System.out.println("name: "+nom);
				System.out.println("atack: "+atack);				
			//}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	
}
