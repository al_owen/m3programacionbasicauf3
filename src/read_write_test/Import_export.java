/**
 * 
 */
package read_write_test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.NumberFormat;
import java.util.Locale;

/**@author Álvaro Owen de la Quintana
 * ProgramacionBasicaUF3 
 * 27 feb 2023
 */
public class Import_export {

	public static void main(String[] args) {
		//variables
		String linea = "";
		String[] datos = {};
		//creamos un formatador de numeros
		NumberFormat nformat = NumberFormat.getInstance(Locale.ITALIAN);
		//pack de objetos para escribir 
		try (BufferedReader bfr = new BufferedReader(new FileReader("res/texto.txt"))){
			while ((linea = bfr.readLine()) != null) {
				datos = linea.split("\\|");//hace un split de la linea cada vez que encuentra un '|'
				int id = Integer.parseInt(datos[0]);//lee el dato en la posicion 0
				System.out.println(id);
				System.out.println(datos[1]);//lee el dato en la posicion 1
				//lee el dato en la posicion 2 y con el formatador lo convierte en decimal adecuandose al formato regional del programa
				//double price = nformat.parse(datos[2]).doubleValue();
				double price = Double.valueOf(datos[2].replace(",", "."));
				System.out.println(price);
				System.out.println(datos[3]);//lee el dato en la posicion 3
			}
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
	}
	
}
