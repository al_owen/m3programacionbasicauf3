package read_write_test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Scanner;

/**
 * @author Álvaro Owen de la Quintana ProgramacionBasicaUF5 14 feb 2023
 */
public class GuardarTextos {

	public static void main(String[] args) {
		//pack de objetos que se debe crear para escribir en un archovo en texto plano
		//se debe un BufferedWriter y le pasamos un FileWriter
		//si en el constructor del fileWriter ponemos un true Ex: "new FileWriter("res/textoConcat.txt", true)"
		//le indicaremos que continue en la siguiente linea, es decir que no borre el contenido anterior
		//caso contrario, le indicaremos que sobrescriba el fichero 
		try (BufferedWriter bfw = new BufferedWriter(new FileWriter("res/text.txt", true))) {
			Scanner sc = new Scanner(System.in);
			String linea = "";

			while (!linea.equalsIgnoreCase("Fin")) {
				linea = sc.nextLine();
				bfw.write(linea);//utilizamos la función write para escribir el texto
				bfw.newLine();//genera un salto de linea

			}
			sc.close();
		} catch (Exception e) {
			System.out.println("Le he fallado, maestro!!!");
		}
		//este es el pack para leer ficheros en texto plano
		try (BufferedReader bfr = new BufferedReader(new FileReader("res/text.txt"))) {
			String linea = "";
			while ((linea = bfr.readLine()) != null) {//si nos devuelve algo quiere decir que hay texto
				System.out.println(linea);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
